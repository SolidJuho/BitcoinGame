﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMDGame
{
    static class Util
    {

        public static Commands TextToCommand(string FirstCommand)
        {
            if (FirstCommand == "status")
            {
                return Commands.Status;
            }
            else if (FirstCommand == "upgrade")
            {
                return Commands.Upgrade;
            }
            else if (FirstCommand == "sell")
            {
                return Commands.Sell;
            }
            else if (FirstCommand == "help")
            {
                return Commands.Help;
            }
            else if (FirstCommand == "clear")
            {
                return Commands.Clear;
            }
            else if (FirstCommand == "buy")
            {
                return Commands.Buy;

            }
            else if (FirstCommand == "transfer")
            {
                return Commands.Transfer;
            }
            else if (FirstCommand == "wallet")
            {
                return Commands.Wallet;
            }
            else if (FirstCommand == "select" || FirstCommand == "switch")
            {
                return Commands.Select;

            }
            else
            {

                return Commands.Unknown;
            }
        }

    }
}
